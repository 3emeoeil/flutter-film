import 'package:flutter/material.dart';
import '/model/movie.dart';

class AddMovie extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        title: Text("Add a movie"),
      ),

      body: Formulaire()
        );
  }
}
class Formulaire extends StatelessWidget {
  final controllerNom = TextEditingController();
  final controllerDate = TextEditingController();
  final controllerDirecteur = TextEditingController();
  final controllerLangue = TextEditingController();
  final controllerGenre = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Form(
      child: ListView(
        children: [
          FormTextField(controller: controllerNom, name: "nom"),
          FormTextField(controller: controllerDate, name: "date"),
          FormTextField(controller: controllerDirecteur, name: "directeur"),
          FormTextField(controller: controllerLangue, name: "langue"),
          FormTextField(controller: controllerGenre, name: "genre"),
          ElevatedButton(
            onPressed: (){
              print(controllerGenre.text);
              Navigator.pop(context,Movie.addMovie(controllerNom.text, controllerDate.text,controllerDirecteur.text, controllerLangue.text, controllerGenre.text));
            },
            child: Text("Ajouter un film"),
        )
        ]
      )
    );
  }

}

class FormTextField extends StatelessWidget {
  final TextEditingController controller;
  final String name;

  const FormTextField({Key? key, required TextEditingController this.controller, required String this.name}) :super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(padding: const EdgeInsets.all(8),
      child :TextFormField(
        controller: controller,
        decoration: InputDecoration(
          labelText: this.name,
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(20))
          ),
        ),
        validator: (value) {
          if (value!.isEmpty) {
            return 'Veuillez remplir ce champs';
          }
          return null;
        },
      ),
    );
  }
}