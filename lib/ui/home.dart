import 'package:flutter/material.dart';
import 'package:films/model/movie.dart';
import 'package:films/ui/addMovie.dart';
import 'package:films/ui/editMovie.dart';


class MovieListView extends StatefulWidget {
  @override
  // State<StatefulWidget> createState() {
  //
  // }

  _MovieListViewState createState() => _MovieListViewState();
}

class _MovieListViewState extends State<MovieListView> {
//class MovieListView extends StatelessWidget {
  //final List<Movie> moviesList = Movie.getMovies();
  List<Movie> moviesList = [];
  //List _movies = ["Film 1", "Film 2", "Film 3", "Film 4", "Film 5", "Film 6"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Movies"),
        backgroundColor: Colors.blueGrey.shade900,
        actions: [
          IconButton(
            icon: Icon(Icons.file_download, color: Colors.white),
            onPressed: () {
              _downloadList();
            },
          ),
          IconButton(
            icon: Icon(Icons.add, color: Colors.white),
            onPressed: (){
              _addMovie(context);
            },

          )
        ],
      ),
      backgroundColor: Colors.blueGrey.shade900,
      body: ListView.builder(
        itemCount: moviesList.length, //nb elements de la liste
        itemBuilder: (BuildContext context, int index) {
          //a modifier par un FuturBuider
          //fonction permettant de construire les widgets de la list
          return Stack(children: [
            movieCard(moviesList[index], context),
            Positioned(top: 10, child:
            movieImage(moviesList[index].images[0]),
            ),
            Positioned(left: 370, top: 0, child:
            IconButton(
              icon: Icon(Icons.delete, color: Colors.white),
              onPressed: (){
                _removeMovie(moviesList[index]);
              },),),
            Positioned(left: 340, top: 0, child:
            IconButton(
              icon: Icon(Icons.edit, color: Colors.white),
              onPressed: (){
                _editMovie(context, moviesList[index]);
              },)
            ),

          ]);
          //   Card(
          //   //plus simple a utiliser que Container
          //   elevation: 5, //ombre
          //   color: Colors.white,
          //   child: ListTile(
          //     leading: CircleAvatar(
          //       //placer un widget a gauche
          //       child: Container(
          //         width: 200,
          //         height: 200,
          //         decoration: BoxDecoration(
          //           //color: Colors.blue,
          //           image: DecorationImage(
          //             image: NetworkImage(moviesList[index].images[0]),
          //             fit: BoxFit.cover
          //           ),
          //           borderRadius: BorderRadius.circular(15),
          //         ),
          //         child: null,//Text("T"),
          //       ),
          //     ),
          //     trailing: Text("..."),
          //     //placer un widget a gauche
          //     title: Text(moviesList[index].title),
          //     subtitle: Text("${moviesList[index].genre}"),
          //     onTap: () {
          //       debugPrint("Movie : ${moviesList.elementAt(index)}");
          //       Navigator.push(
          //           context,
          //           MaterialPageRoute(
          //               builder: (context) => MovieDetails(movie:
          //                     moviesList.elementAt(index),
          //                   ))); //Navigator permet de changer de route
          //     },
          //   ),
          // );
        },
      ),
    );
  }

  Widget movieCard(Movie movie, BuildContext context) {
    return InkWell(
        child: Container(
          margin: EdgeInsets.only(left: 60),
          width: MediaQuery
              .of(context)
              .size
              .width,
          height: 120,
          child: Card(
            color: Colors.black38,
            child: Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 8.0, left: 54.0),
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  //widgets se positionnent a partir de la gauche
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Text(movie.title,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17,
                                  color: Colors.white)),
                        ),
                        Text(
                          "Rating: ${movie.imdbRating} / 10",
                          style: mainStyle(),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text("Released: ${movie.released}", style: mainStyle()),
                        Text(movie.runtime, style: mainStyle()),
                        Text(movie.rated, style: mainStyle())
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        onTap: () {
          debugPrint("Movie : ${movie.title}");
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      MovieDetails(
                        movie: movie,
                      ))); //Navigator permet de changer de route
        });
  }

  TextStyle mainStyle() {
    return TextStyle(fontSize: 15, color: Colors.grey);
  }

  Widget movieImage(String url) {
    return Container(
      width: 100,
      height: 100,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        image: DecorationImage(
            image: NetworkImage(url ??
                'https://assets.stickpng.com/thumbs/5a4613e5d099a2ad03f9c995.png'),
            fit: BoxFit.cover),
      ),
    );
  }

  void _downloadList() {
    Movie.loadMovies().then((m) {
      setState(() {
        if (m != null) {
          m.forEach((item) => moviesList.add(item));
        } else {
          moviesList = [];
        }
      });
    });
  }

  void _addMovie(BuildContext context) async{
    final newMovie = await Navigator.push(context,
        MaterialPageRoute(builder: (context) => AddMovie()));
    setState(() {
      moviesList.add(newMovie);
    });
  }

  void _removeMovie(Movie movie) async {
    setState(() {
      moviesList.removeWhere((item) => item.title == movie.title);
    });
  }

  void _editMovie(BuildContext context, Movie movie) async {
    final editMovie = await Navigator.push(context,
        MaterialPageRoute(builder: (context) => EditMovie(movie)));
    setState(() {
      moviesList.add(editMovie);
      _removeMovie(movie);
    });
  }
}

class MovieDetails extends StatelessWidget {
  //final String movieName;
  final Movie movie;

  const MovieDetails({Key? key, required this.movie})
      : super(key: key); //const MovieDetails({Key key, this.movieName})
  //    : super(key: key); //parametres entre {} deviennent optionnels
  //key est utilise pour renseigner la position dans l'arbre.

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${movie.title} details"),
        backgroundColor: Colors.blueGrey.shade900,
      ),
      body: ListView(
        children: [
          DetailsHaut(image: movie.images[0]),
          DetailMilieu(movie: movie,),
          HorizontalLine(),
          MovieCast(movie),
          HorizontalLine(),
          DetailFin(liens: movie.images,)
        ],
        // body: Container(
        //   child: Center(
        //     child: RaisedButton(
        //       child: Text("Go Back"),
        //       onPressed: () {
        //         Navigator.pop(context);
        //       },
        //     ),
        //   ),
      ),
    );
  }
}

class DetailsHaut extends StatelessWidget {
  final String image;

  const DetailsHaut({Key? key, required this.image}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        Stack(alignment: Alignment.center, children: [
          Container(
            width: MediaQuery
                .of(context)
                .size
                .width,
            height: 170,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(image), fit: BoxFit.cover)),
          ),
          Icon(
            Icons.play_circle_outline,
            size: 100,
            color: Colors.white,
          ),
        ]),
        Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Color(0x00f5f5f5), Color(0xfff5f5f5)],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter),

          ),
          height: 80,
        ),
      ],
    );
  }
}

class DetailMilieu extends StatelessWidget {
  final Movie movie;


  const DetailMilieu({Key? key, required this.movie}) :super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Row(
        children: [
          MoviePoster(movie.images[0].toString()),
          SizedBox(width: 16,),
          Expanded(
            child: DetailFilm(movie),
          )
        ],
      ),
    );
  }

}

class DetailFilm extends StatelessWidget {
  final Movie movie;


  DetailFilm(this.movie);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("${movie.year} . ${movie.genre}".toUpperCase(),
          style: TextStyle(
              fontWeight: FontWeight.w400,
              color: Colors.cyan
          ),),
        Text(movie.title,
          style: TextStyle(fontWeight: FontWeight.w500, fontSize: 32),),
        Text.rich(TextSpan(
            style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
            children: <TextSpan>[
              TextSpan(
                  text: movie.plot
              ),
              TextSpan(
                  text: "More...",
                  style: TextStyle(
                      color: Colors.indigo
                  )
              )
            ]))
      ],
    );
  }
}

class MoviePoster extends StatelessWidget {
  final String poster;

  MoviePoster(this.poster);

  @override
  Widget build(BuildContext context) {
    var borderRadius = BorderRadius.all(Radius.circular(10));
    return Card(
      child: ClipRRect(
          borderRadius: borderRadius,
          child: Container(
            width: MediaQuery
                .of(context)
                .size
                .width / 4,
            height: 160,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(poster), fit: BoxFit.cover)),
          )
      ),
    );
  }


}


class MovieCast extends StatelessWidget{
  final Movie movie;


  MovieCast(this.movie);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          MovieField(field:"Cast",value:movie.actors),
          MovieField(field:"Directors",value:movie.director),
          MovieField(field: "Awards",value: movie.awards,)
        ],
      ),
    );
  }

}

class MovieField extends StatelessWidget{
  final String field;
  final String value;


  const MovieField({Key? key,required this.field, required this.value}):super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("$field : ",style: TextStyle(
            color: Colors.black38,
            fontSize: 12,
            fontWeight: FontWeight.w300
        ),),
        Expanded(
          child: Text(value,style: TextStyle(
              color: Colors.black,fontSize: 12,fontWeight: FontWeight.w300
          ),),
        )
      ],
    );
  }
}

class HorizontalLine extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal:16.0,vertical:12),
      child: Container(
        height: 0.5,
        color: Colors.grey,
      ),
    );
  }

}

class DetailFin extends StatelessWidget{
  final List<String> liens;

  const DetailFin({Key? key, required this.liens}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left:8.0),
          child: Text("More Movie Posters".toUpperCase(),
            style:TextStyle(
                fontSize: 14,
                color: Colors.black26
            )
            ,),
        ),
        Container(
          height:170,
          padding: EdgeInsets.symmetric(vertical: 16),
          child: ListView.separated(
            scrollDirection: Axis.horizontal,
            separatorBuilder: (context,index) => SizedBox(width: 8,),
            itemCount: liens.length,
            itemBuilder: (context,index) => ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Container(
                width: MediaQuery.of(context).size.width / 4,
                height: 160,
                decoration: BoxDecoration(
                  image: DecorationImage(image: NetworkImage(liens[index]),
                      fit:BoxFit.cover),
                ),
              ),
            ),
          ),
        )

      ],
    );
  }

}

